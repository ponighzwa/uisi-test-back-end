<div class="modal fade text-left" id="create-folder-modal" tabindex="-1" role="dialog" aria-labelledby="create-folder-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="create-folder-modal">Create Folder</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-folder-form" class="form form-horizontal">
                    <div class="form-body">
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="folder_name">Folder Name</label>
                            <div class="col-md-9">
                                <input type="text" id="folder_name" class="form-control" placeholder="Optional"
                                       name="folder_name">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="create-folder-button" class="btn btn-outline-primary">Create Folder</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="update-folder-modal" tabindex="-1" role="dialog" aria-labelledby="update-folder-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="update-folder-modal">Update Folder</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="update-folder-form" class="form form-horizontal">
                    <div class="form-body">
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="folder_name">Folder Name</label>
                            <div class="col-md-9">
                                <input type="text" id="id_update" class="form-control" placeholder="Optional"
                                       name="id_update" hidden>
                                <input type="text" id="folder_name_update" class="form-control" placeholder="Optional"
                                       name="folder_name">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="update-folder-button" class="btn btn-outline-primary">Update Folder</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="file-upload-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Upload File</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="upload-file-form" class="form form-horizontal">
                    <div class="form-body">
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="file_name">File Name</label>
                            <div class="col-md-9">
                                <input type="text" id="file_name" class="form-control" placeholder="Optional"
                                       name="file_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 label-control">Select File</label>
                            <div class="col-md-9">
                                <label id="file-label" class="file center-block">
                                    <input type="file" id="file">
                                    <span class="file-custom"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="upload-button" class="btn btn-outline-primary">Upload File</button>
            </div>
        </div>
    </div>
</div>
