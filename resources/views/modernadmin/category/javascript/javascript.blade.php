<script>
    $( document ).ready(function() {
        let table = $("#main-table").DataTable({
            processing: true,
            serverSide: true,
            paging: false,
            searching: false,
            bInfo: false,
            language: {
                processing: "Please Wait",
            },
            ajax:{
                url: "{{ route('category.read')}}",
                dataType: "json",
                type: "GET"
            },
            columns: [
                { data: 'name',name:'name'},
                { data: 'description', name:'description'},
                { data: 'action', name:'action'},
            ]
        }).on("click", ".update-button", function(){
            let tr = $(this).closest("tr");
            let row = $("#main-table").DataTable().row(tr).data();
            $("#id_update").val(row.id);
            console.log(row.id);
            $.ajax({
                url : "{{ route('category.read')}}" + "/" + row.id,
                type: "get",
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $("#name_update").val(row.name);
                        $("#description_update").val(row.description);
                    } else{
                        toastr.warning(data.error_message, 'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }
            });

            $('#update-category-modal').modal('show');
        }).on("click", ".delete-button", function(){
            let tr = $(this).closest("tr");
            let row = $("#main-table").DataTable().row(tr).data();
            $("#id_delete").val(row.id);
            console.log(row.id);

            $('#delete-category-modal').modal('show');
        });

        $( "#create-category-button" ).click(function() {
            let formData = new FormData();
            formData.append('name', $('#name_create').val());
            formData.append('description', $('#description_create').val());
            $.ajax({
                url : "{{ route('category.create')}}",
                type: "POST",
                data : formData,
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $('#create-category-form').trigger("reset");
                        $('#create-category-modal').modal('hide');

                        table.ajax.reload();
                        toastr.success('Your Data has been saved', 'Success');
                    } else{
                        toastr.warning(data.error_message, 'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }
            });
        });

        $( "#update-category-button" ).click(function() {
            let formData = new FormData();
            formData.append('name', $('#name_update').val());
            formData.append('description', $('#description_update').val());
            formData.append('_method', "PUT");
            $.ajax({
                url : "{{ route('category.create')}}" + "/" + $('#id_update').val(),
                type: "POST",
                data : formData,
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $('#update-category-form').trigger("reset");
                        $('#update-category-modal').modal('hide');

                        table.ajax.reload();
                        toastr.success('Your Data has been updated', 'Success');
                    } else{
                        toastr.warning(data.error_message, 'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }
            });
        });

        $( "#delete-category-button" ).click(function() {
            $.ajax({
                url : "{{ route('category.read')}}" + "/" + $('#id_delete').val(),
                type: "DELETE",
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $('#delete-category-form').trigger("reset");
                        $('#delete-category-modal').modal('hide');

                        table.ajax.reload();
                        toastr.success('Your Data has been deleted', 'Success');
                    } else{
                        toastr.warning(data.error_message, 'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }
            });
        });
    });
</script>
