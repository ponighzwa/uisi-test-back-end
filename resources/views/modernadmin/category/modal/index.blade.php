<div class="modal fade text-left" id="create-category-modal" tabindex="-1" role="dialog" aria-labelledby="create-category-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="create-category-modal">Create Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-category-form" class="form form-horizontal">
                    <div class="form-body">
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="name">Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name_create" class="form-control" placeholder="Category Name"
                                       name="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="description">Description</label>
                            <div class="col-md-9">
                                <textarea type="text" id="description_create" class="form-control" placeholder="Description"
                                          name="description"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="create-category-button" class="btn btn-outline-primary">Create Category</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="update-category-modal" tabindex="-1" role="dialog" aria-labelledby="update-category-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="update-category-modal">Update Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="update-category-form" class="form form-horizontal">
                    <div class="form-body">
                        <input type="text" id="id_update" class="form-control" name="name" hidden>
                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="name">Name</label>
                            <div class="col-md-9">
                                <input type="text" id="name_update" class="form-control" placeholder="Category Name"
                                       name="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 label-control" for="description">Description</label>
                            <div class="col-md-9">
                                <textarea type="text" id="description_update" class="form-control" placeholder="Description"
                                          name="description"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="update-category-button" class="btn btn-outline-primary">Update Category</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="delete-category-modal" tabindex="-1" role="dialog" aria-labelledby="delete-category-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="delete-category-modal">Delete Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="delete-category-form" class="form form-horizontal">
                    <div class="form-body">
                        <input type="text" id="id_delete" class="form-control" name="name" hidden>
                    </div>
                </form>
                <p>Are you sure want to delete this data?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="delete-category-button" class="btn btn-outline-primary">Delete Category</button>
            </div>
        </div>
    </div>
</div>
