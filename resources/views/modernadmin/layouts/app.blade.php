<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    @include("modernadmin.layouts.header")
    @include("modernadmin.layouts.css")
</head>
<body class="horizontal-layout horizontal-menu 2-columns menu-expanded" data-open="hover"
      data-menu="horizontal-menu" data-col="2-columns">
    @include("modernadmin.layouts.navbar.app")
    @include("modernadmin.layouts.menu")

    @yield('content')

    @include("modernadmin.layouts.footer")
    @include("modernadmin.layouts.javascript")
</body>
</html>
