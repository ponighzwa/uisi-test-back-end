<link rel="apple-touch-icon" href="{{asset("uisi.png")}}">
<link rel="shortcut icon" type="image/x-icon" href="{{asset("uisi.png")}}">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
      rel="stylesheet">
<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
      rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/vendors.css")}}">
@yield('custom_css')
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/app.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/core/menu/menu-types/horizontal-menu.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/core/colors/palette-gradient.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/vendors/css/charts/morris.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/fonts/simple-line-icons/style.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/core/colors/palette-gradient.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/fonts/line-awesome/css/line-awesome.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/fonts/line-awesome/css/line-awesome-font-awesome.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/assets/css/style.css")}}">
