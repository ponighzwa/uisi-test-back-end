<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow"
     role="navigation" data-menu="menu-wrapper">
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="" data-toggle="dropdown"><i class="la la-building"></i>
                    <span>Building</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu=""><a class="dropdown-item" href="{{route('building.index')}}" data-toggle="dropdown">
                            Building List</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class="dropdown-toggle nav-link" href="" data-toggle="dropdown"><i class="la la-list"></i>
                    <span>Category</span>
                </a>
                <ul class="dropdown-menu">
                    <li data-menu=""><a class="dropdown-item" href="{{route('category.index')}}" data-toggle="dropdown">
                            Category List</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
