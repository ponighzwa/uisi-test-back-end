<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul class="navigation navigation-main">
            @foreach($data->menu as $menu)
                <li class="nav-item {{ isset($menu->is_active) ? 'active' : '' }}"><a href=""><i class="{{$menu->icon}}"></i>
                        <span class="menu-title">{{$menu->name}}</span></a>
                    <ul class="menu-content">
                        @isset($menu->child)
                            @foreach($menu->child as $child)
                                <li><a class="menu-item {{ isset($child->is_active) ? 'active' : '' }}"
                                       href="">{{$child->name}}</a>
                                </li>
                            @endforeach
                        @endisset
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>
