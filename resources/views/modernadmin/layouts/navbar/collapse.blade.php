<ul class="nav navbar-nav mr-auto float-left">
    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
    <li class="dropdown nav-item mega-dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">Mega</a>
        @include("modernadmin.layouts.navbar.dropdown")
    </li>
    <li class="nav-item nav-search"><a class="nav-link nav-link-search" href="#"><i class="ficon ft-search"></i></a>
        @include("modernadmin.layouts.navbar.search")
    </li>
</ul>
