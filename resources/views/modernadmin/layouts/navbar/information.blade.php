<ul class="nav navbar-nav float-right">
    <li class="dropdown dropdown-user nav-item">
        @include("modernadmin.layouts.navbar.profile")
    </li>
    <li class="dropdown dropdown-language nav-item">
        @include("modernadmin.layouts.navbar.language")
    </li>
    <li class="dropdown dropdown-notification nav-item">
        @include("modernadmin.layouts.navbar.notification")
    </li>
    <li class="dropdown dropdown-notification nav-item">
        @include("modernadmin.layouts.navbar.message")
    </li>
</ul>
