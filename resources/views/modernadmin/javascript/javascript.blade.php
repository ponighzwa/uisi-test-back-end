<script>
    $( document ).ready(function() {
        // if (!lscache.supported()) {
        //     alert('Please use Chrome or Mozilla to continue');
        // }
        // lscache.flushExpired();
        // console.log(lscache.get('current_directory'));
        // let active_directory = (lscache.get('current_directory') === null) ? 0 : lscache.get('current_directory');
        $('.update-folder-button').hide();
        let active_directory = localStorage.getItem("current_directory");
        if (active_directory === '0'){
            $('.update-folder-button').hide();
        }
        function refresh_datatable(parent_id) {
            table.clear();
            table.destroy();
            table = $("#main-table").DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching: false,
                bInfo: false,
                language: {
                    processing: "Please Wait",
                    sZeroRecords: "This is an empty Directory"
                },
                ajax:{
                    url: "{{ route('directory.list')}}",
                    dataType: "json",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        parent_id: parent_id
                    },
                },
                drawCallback: function(result) {
                    $('.add-folder-button').show();
                    $('.add-file-button').show();
                    $('.update-folder-button').show();
                    $(".breadcrumb").empty();
                    localStorage.setItem("current_directory", parent_id);
                    $.each(result.json.breadcrumb, function( index, value ) {
                        $(".breadcrumb").append('<li class="breadcrumb-item" value="'+ value.id +'"><a>' +
                            value.name +'</a></li>');
                    });
                    if (!result.json.permission.create){
                        $('.add-folder-button').hide();
                        $('.add-file-button').hide();
                    }
                    if (!result.json.permission.update){
                        $('.update-folder-button').hide();
                    }
                    //do whatever
                },
                columns: [
                    { data: 'icon',name:'icon'},
                    { data: 'name',name:'name', width:"70%"},
                    { data: 'created_at', name:'created_at'},
                    { data: 'updated_at',name:'updated_at' },
                    { data: 'action',name:'action', width:"20%", className: "text-center"},
                ]
            })
        }

        function download_file(id) {
            window.open("{{route("download.file")}}"+"?id="+id);
        }

        let table = $("#main-table").DataTable({
            processing: true,
            serverSide: true,
            paging: false,
            searching: false,
            bInfo: false,
            language: {
                processing: "Please Wait",
                sZeroRecords: "This is an empty Directory"
            },
            ajax:{
                url: "{{ route('directory.list')}}",
                dataType: "json",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    parent_id: active_directory
                },
            },
            drawCallback: function(result) {
                // lscache.set('current_directory', 0, 300);
                localStorage.setItem("current_directory", '0');
                $.each(result.json.breadcrumb, function( index, value ) {
                    $(".breadcrumb").append('<li class="breadcrumb-item" value="'+ value.id +'"><a>' +
                        value.name +'</a></li>');
                });
                //do whatever
            },
            columns: [
                { data: 'icon',name:'icon'},
                { data: 'name',name:'name', width:"70%"},
                { data: 'created_at', name:'created_at'},
                { data: 'updated_at',name:'updated_at' },
                { data: 'action',name:'action', width:"20%", className: "text-center"},
            ]
        }).on("click", ".update-button", function(){
            let tr = $(this).closest("tr");
            let row = $("#main-table").DataTable().row(tr).data();
            $("#id_update").val(row.id);
            $('#update-folder-modal').modal('show');
        });

        $('#update-folder-modal').on('hidden.bs.modal', function () {
            $("#id_update").val(null);
        });

        $('#main-table tbody').on('dblclick', 'tr', function () {
            let data = table.row( this ).data();
            if (data != null && data.type==='folder'){
                refresh_datatable(data.id);
            } else{
                download_file(data.id);
            }
        });
        $("ol.breadcrumb").on("click","li", function(){
            refresh_datatable($(this).attr('value'))
        });

        $( "#upload-button" ).click(function() {
            let formData = new FormData();
            let file = $('#file')[0].files[0];
            if (file != null){
                formData.append('file', file);
            }
            formData.append('file_name', $('#file_name').val());
            formData.append('parent_id', localStorage.getItem("current_directory"));
            formData.append('_token', "{{ csrf_token() }}");
            $.ajax({
                url : "{{ route('upload.file')}}",
                type: "POST",
                data : formData,
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $('#upload-file-form').trigger("reset");
                        $('#file-upload-modal').modal('hide');
                        refresh_datatable(localStorage.getItem("current_directory"));
                        toastr.success('Your file has been uploaded','Success');
                    } else{
                        toastr.warning(data.error_message,'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }

            });
        });
        $( "#create-folder-button" ).click(function() {
            let formData = new FormData();
            formData.append('name', $('#folder_name').val());
            formData.append('parent_id', localStorage.getItem("current_directory"));
            formData.append('_token', "{{ csrf_token() }}");
            $.ajax({
                url : "{{ route('directory.create')}}",
                type: "POST",
                data : formData,
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $('#create-folder-form').trigger("reset");
                        $('#create-folder-modal').modal('hide');
                        refresh_datatable(localStorage.getItem("current_directory"));
                        toastr.success('New folder has been created', 'Success');
                    } else{
                        toastr.warning(data.error_message, 'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }
            });
        });

        $( "#update-folder-button" ).click(function() {
            let formData = new FormData();
            let id_send = $('#id_update').val() ? $('#id_update').val()
                : localStorage.getItem("current_directory");
            formData.append('name', $('#folder_name_update').val());
            formData.append('_token', "{{ csrf_token() }}");
            formData.append('_method', "PUT");
            $.ajax({
                url : "{{ route('directory.create')}}" + "/" + id_send,
                type: "POST",
                data : formData,
                processData: false,
                contentType: false,
                success : function(data) {
                    if (data.status){
                        $('#update-folder-form').trigger("reset");
                        $('#update-folder-modal').modal('hide');
                        refresh_datatable(localStorage.getItem("current_directory"));
                        toastr.success('Your Update has been saved', 'Success');
                    } else{
                        toastr.warning(data.error_message, 'Warning');
                    }
                },
                error: function (data) {
                    toastr.error('Please contact IT if this keep happening','Error');
                }
            });
        });
    });
</script>
