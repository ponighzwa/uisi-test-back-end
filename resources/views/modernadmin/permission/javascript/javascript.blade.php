<script>
    $( document ).ready(function() {
        let table = $("#main-table").DataTable({
            processing: true,
            serverSide: true,
            language: {
                processing: "Please Wait",
                sZeroRecords: "This is an empty Directory"
            },
            ajax:{
                url: "{{ route('permission-folder.user-list')}}",
                dataType: "json",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                },
            },
            columns: [
                { data: 'name',name:'name'},
                { data: 'email',name:'email'},
                { data: 'created_at', name:'created_at'},
                { data: 'updated_at',name:'updated_at' },
                {
                    data: null,
                    sortable: false,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="{{route("permission-folder.user-list")}}/'+data.id+'"><button type="button" ' +
                            'class="btn btn-info mr-1 mb-1 btn-sm">Permission</button></a>';
                    },
                }
            ]
        });
    });
</script>
