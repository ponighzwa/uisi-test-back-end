<script>
    $( document ).ready(function() {
        let table = $("#main-table").DataTable({
            processing: true,
            serverSide: true,
            language: {
                processing: "Please Wait",
                sZeroRecords: "This is an empty Directory"
            },
            ajax:{
                url: "{{ route('permission-folder.folder-list')}}",
                dataType: "json",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    user_id: $("input[name=user_id]").val()
                },
            },
            columns: [
                { data: 'name',name:'name'},
                { data: 'read',name:'read'},
                { data: 'create', name:'create'},
                { data: 'write',name:'write'},
                { data: 'delete',name:'delete'}
            ]
        });
    }).on("click", ".read-checkbox", function(){
        let value;
        if($(this).prop('checked') === true){
            value = true;
        } else {
            value = false;
        }
        var tr = $(this).closest("tr");
        var row = $("#main-table").DataTable().row(tr).data();
        $.ajax({
            url : "{{ route('permission-folder.update-user-permission')}}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                _token : "{{ csrf_token() }}",
                directory_id : row.id,
                action: "read",
                value: value,
                user_id: $("input[name=user_id]").val()
            }),
            success : function(data) {
                if (data.status){
                    toastr.success('Permission has been updated','Success');
                } else{
                    toastr.warning(data.error_message,'Warning');
                }
            },
            error: function (data) {
                toastr.error('Please contact IT if this keep happening','Error');
            }
        });
    }).on("click", ".create-checkbox", function(){
        let value;
        if($(this).prop('checked') === true){
            value = true;
        } else {
            value = false;
        }
        var tr = $(this).closest("tr");
        var row = $("#main-table").DataTable().row(tr).data();
        $.ajax({
            url : "{{ route('permission-folder.update-user-permission')}}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                _token : "{{ csrf_token() }}",
                directory_id : row.id,
                action: "create",
                value: value,
                user_id: $("input[name=user_id]").val()
            }),
            success : function(data) {
                if (data.status){
                    toastr.success('Permission has been updated','Success');
                } else{
                    toastr.warning(data.error_message,'Warning');
                }
            },
            error: function (data) {
                toastr.error('Please contact IT if this keep happening','Error');
            }
        });
    }).on("click", ".write-checkbox", function(){
        let value;
        if($(this).prop('checked') === true){
            value = true;
        } else {
            value = false;
        }
        var tr = $(this).closest("tr");
        var row = $("#main-table").DataTable().row(tr).data();
        $.ajax({
            url : "{{ route('permission-folder.update-user-permission')}}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                _token : "{{ csrf_token() }}",
                directory_id : row.id,
                action: "update",
                value: value,
                user_id: $("input[name=user_id]").val()
            }),
            success : function(data) {
                if (data.status){
                    toastr.success('Permission has been updated','Success');
                } else{
                    toastr.warning(data.error_message,'Warning');
                }
            },
            error: function (data) {
                toastr.error('Please contact IT if this keep happening','Error');
            }
        });
    }).on("click", ".delete-checkbox", function(){
        let value;
        if($(this).prop('checked') === true){
            value = true;
        } else {
            value = false;
        }
        var tr = $(this).closest("tr");
        var row = $("#main-table").DataTable().row(tr).data();
        $.ajax({
            url : "{{ route('permission-folder.update-user-permission')}}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                _token : "{{ csrf_token() }}",
                directory_id : row.id,
                action: "delete",
                value: value,
                user_id: $("input[name=user_id]").val()
            }),
            success : function(data) {
                if (data.status){
                    toastr.success('Permission has been updated','Success');
                } else{
                    toastr.warning(data.error_message,'Warning');
                }
            },
            error: function (data) {
                toastr.error('Please contact IT if this keep happening','Error');
            }
        });
    });
</script>
