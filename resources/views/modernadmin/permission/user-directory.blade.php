@extends("modernadmin.layouts.app")

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/fonts/simple-line-icons/style.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/vendors/css/tables/datatable/datatables.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/vendors/css/extensions/toastr.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/plugins/extensions/toastr.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/page/app.css")}}">
@endsection

@section('custom_js')
    <script src="{{asset("theme/modernadmin/app-assets/vendors/js/tables/datatable/datatables.min.js")}}" type="text/javascript"></script>
    <script src="{{asset("theme/modernadmin/app-assets/vendors/js/extensions/toastr.min.js")}}" type="text/javascript"></script>
    @include("modernadmin.permission.javascript.user-directory")
@endsection

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">{{$data->data->name}} User Folder Permission</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route("permission-folder.index")}}">User List</a>
                            </li>
                            <li class="breadcrumb-item">Folder Permission
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <input type="text" name="user_id" value="{{$data->data->id}}" hidden/>
                                <table id="main-table" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Read</th>
                                        <th>Create</th>
                                        <th>Write</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Read</th>
                                        <th>Create</th>
                                        <th>Write</th>
                                        <th>Delete</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
