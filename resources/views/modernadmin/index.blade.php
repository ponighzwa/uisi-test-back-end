@extends("modernadmin.layouts.app")

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/fonts/simple-line-icons/style.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/vendors/css/tables/datatable/datatables.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/vendors/css/extensions/toastr.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/app-assets/css/plugins/extensions/toastr.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("theme/modernadmin/page/app.css")}}">
@endsection

@section('custom_js')
    <script src="{{asset("theme/modernadmin/app-assets/vendors/js/tables/datatable/datatables.min.js")}}" type="text/javascript"></script>
    <script src="{{asset("theme/modernadmin/app-assets/vendors/js/extensions/toastr.min.js")}}" type="text/javascript"></script>
    {{--@include("modernadmin.javascript.javascript")--}}
@endsection

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Directory List</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard">
                                <button type="button" class="btn btn-success mr-1 mb-1 btn-sm add-folder-button"
                                        data-toggle="modal" data-target="#create-folder-modal">
                                    <i class="ft-folder"></i>&nbsp;&nbsp;Add Folder</button>
                                <table id="main-table" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--@include("modernadmin.home-modal.index")--}}
@endsection
