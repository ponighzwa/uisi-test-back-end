<?php

namespace App\Model;

use App\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use ModelTraits;

    protected $table = 'category';
    protected $guarded = ["id"];

    private $validation_array = [];
    private $return;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->return = new \stdClass();
    }

    public function setValidationArray(array $validation_array): void
    {
        $this->validation_array = $validation_array;
    }

    public function read($id = null){
        try{
            $query = Category::select("id", "name", "description")->where("is_delete", false);

            if ($id != null){
                $query = $query->where("id", $id);
            }
            $this->return->count = $query->count();
            $this->return->data = $query->get();

            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }

    public function store($request){
        try{
            $this->setValidationArray([
                'name' => 'required|max:50',
                'description' => 'nullable'
            ]);

            $this->return = $this->validation_process($request,$this->validation_array,["name", "description"]);
            if (!$this->return->status){
                return $this->return;
            }
            Category::create($this->return->input);
            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }

    public function put($id, $request){
        try{
            $this->setValidationArray([
                'name' => 'required|max:50',
                'description' => 'nullable'
            ]);

            $this->return = $this->validation_process($request,$this->validation_array,["name", "description"]);
            if (!$this->return->status){
                return $this->return;
            }
            Category::where("id", $id)->update($this->return->input);
            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }

    public function remove($id){
        try{
            Category::where("id", $id)->update(["is_delete" => true]);
            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }
}
