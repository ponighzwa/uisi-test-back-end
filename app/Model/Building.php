<?php

namespace App\Model;

use App\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Building extends Model
{
    use ModelTraits;

    protected $table = 'building';
    protected $guarded = ["id"];

    private $validation_array = [];
    private $return;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->return = new \stdClass();
    }

    public function setValidationArray(array $validation_array): void
    {
        $this->validation_array = $validation_array;
    }

    public function read($id = null){
        try{
            $query = DB::table("building")->leftJoin("category","category.id","=",
                "building.category_id")->leftJoin("building as parent","parent.id","=",
                "building.parent_id")->select("building.id", "building.name", "building.description",
                "category.id as category_id", "category.name as category_name", "parent.name as parent_name",
                "building.parent_id as parent_id")
                ->where("building.is_delete", false);

            if ($id != null){
                $query = $query->where("building.id", $id);
            }

            $this->return->data = $query->get();

            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }

    public function store($request){
        try{
            $this->setValidationArray([
                'name' => 'required|max:50',
                'category_id' => 'required',
                'description' => 'nullable',
                'parent_id' => 'nullable'
            ]);

            $this->return = $this->validation_process($request,$this->validation_array,["name", "description",
                "parent_id","category_id"]);
            if (!$this->return->status){
                return $this->return;
            }
            Building::create($this->return->input);
            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }

    public function put($id, $request){
        try{
            $this->setValidationArray([
                'name' => 'required|max:50',
                'category_id' => 'required',
                'description' => 'nullable',
                'parent_id' => 'nullable'
            ]);

            $this->return = $this->validation_process($request,$this->validation_array,["name", "description",
                "parent_id","category_id"]);
            if (!$this->return->status){
                return $this->return;
            }
            Building::where("id", $id)->update($this->return->input);
            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }

    public function remove($id){
        try{
            Building::where("id", $id)->update(["is_delete" => true]);
            $this->return->status = true;
        } catch (\Exception $exception){
            $this->return->status = false;
            $this->return->error_message = $exception->getMessage();
            $this->return->error_code = $this->error_exception_code;
        }

        return $this->return;
    }
}
