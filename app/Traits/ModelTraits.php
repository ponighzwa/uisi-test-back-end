<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 18/10/2020
 * Time: 08.53
 */
namespace App\Traits;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait ModelTraits
{
    protected $error_filter_code = "error_filter";
    protected $error_exception_code = "error_exception";
    protected $error_message_exception = "Failed to Save data";
    protected $error_message_query = "Query error";

    protected $default_ordering_direction = "ASC";
    protected $default_ordering_column = "id";

    private $input = [];
    protected function validation_process(Request $request,Array $validation_array,Array $getter = array()){
        $this->input = [];
        $data = new \stdClass();
        $validator = Validator::make($request->all(), $validation_array);

        if ($validator->fails()) {
            $data->status = false;
            $data->error_input = $validator->errors();
            $data->error_field = $validator->errors()->keys();
            $data->error_message = $validator->errors()->all()[0];
            $data->error_code = $this->error_filter_code;
            return $data;
        }

        $array_keys = empty($getter) ? array_keys($validation_array) : $getter;
        foreach ($array_keys as $array_key){
            $this->input [$array_key] = $request->input($array_key);
        }
        $this->input["created_at"] = Carbon::now();
        $this->input["updated_at"] = Carbon::now();
        $this->input["is_delete"] = false;
        $data->input = $this->input;
        $data->status = true;

        return $data;
    }
}
