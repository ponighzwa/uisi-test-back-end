<?php

namespace App\Http\Controllers;

use App\Model\Building;
use Illuminate\Http\Request;

class BuildingController extends Controller
{

    public function read(){
        $building = new Building();
        $return = $building->read();

        return response()->json($return, 200);
    }

    public function show($id){
        $building = new Building();
        $return = $building->read($id);

        return response()->json($return, 200);
    }

    public function create(Request $request){
        $building = new Building();
        $return = $building->store($request);

        return response()->json($return, 200);
    }

    public function put(Request $request, $id){
        $building = new Building();
        $return = $building->put($id, $request);

        return response()->json($return, 200);
    }

    public function remove($id){
        $building = new Building();
        $return = $building->remove($id);

        return response()->json($return, 200);
    }
}
