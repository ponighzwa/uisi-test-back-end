<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function read(Request $request){
        $category = new Category();
        $return = $category->read();

        foreach ($return->data as $datum){
            $datum->action = '';

            $datum->action .= '<button type="button" class="btn btn-warning mr-1 mb-1 btn-sm update-button"><i class="ft-edit-2"></i>  Update</button>';
            $datum->action .= '<button type="button" class="btn btn-danger mr-1 mb-1 btn-sm delete-button"><i class="ft-trash-2"></i> Delete</button>';
        }

        $data = new \stdClass();

        $data->draw = $request->input('draw');
        $data->recordsTotal = $return->count;
        $data->recordsFiltered = $return->count;
        $data->data = $return->data->toArray();
        return response()->json($data, 200);
    }

    public function show($id){
        $category = new Category();
        $return = $category->read($id);

        return response()->json($return, 200);
    }

    public function create(Request $request){
        $category = new Category();
        $return = $category->store($request);

        return response()->json($return, 200);
    }

    public function put(Request $request, $id){
        $category = new Category();
        $return = $category->put($id, $request);

        return response()->json($return, 200);
    }

    public function remove($id){
        $category = new Category();
        $return = $category->remove($id);

        return response()->json($return, 200);
    }


    public function index(){
        return view('modernadmin/category/index');
    }
}
