<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->char("name",50);
            $table->text("description")->nullable();

            $table->boolean("is_delete")->default(true);
            $table->timestamps();
        });

        Schema::table('building', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('building');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('building', function (Blueprint $table) {
            $table->dropForeign('building_category_id_foreign');
        });
        Schema::dropIfExists('category');
    }
}
