/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.34-MariaDB : Database - uisi-backend
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uisi-backend` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `uisi-backend`;

/*Table structure for table `building` */

DROP TABLE IF EXISTS `building`;

CREATE TABLE `building` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_delete` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `building_category_id_foreign` (`category_id`),
  CONSTRAINT `building_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `building` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `building` */

insert  into `building`(`id`,`parent_id`,`name`,`description`,`is_delete`,`created_at`,`updated_at`,`category_id`) values 
(1,NULL,'Gedung utama',NULL,0,'2021-02-18 13:02:02','2021-02-18 13:02:02',1),
(2,1,'Lapangan Basket',NULL,0,'2021-02-18 13:02:37','2021-02-18 13:02:37',2),
(3,2,'Lapangan futsal new','ini deskripsi',1,'2021-02-18 13:13:39','2021-02-18 13:14:28',2);

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_delete` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`description`,`is_delete`,`created_at`,`updated_at`) values 
(1,'Building','To tell building type',0,'2021-02-18 12:19:09','2021-02-18 12:22:29'),
(2,'Lapangan','To tell the lapangan type',0,'2021-02-18 12:05:03','2021-02-18 12:05:03'),
(3,'Testing',NULL,0,'2021-02-18 14:05:27','2021-02-18 14:05:27'),
(4,'Testing','<p>testing cuy</p>',0,'2021-02-18 14:05:28','2021-02-18 14:05:28'),
(5,'Testing','<p>testing cuy</p>',0,'2021-02-18 14:05:28','2021-02-18 14:05:28'),
(6,'Testing','<p>testing cuy</p>',0,'2021-02-18 14:05:29','2021-02-18 14:05:29'),
(7,'Umy Rizqi',NULL,0,'2021-02-18 14:06:35','2021-02-18 14:06:35'),
(8,'wew',NULL,0,'2021-02-18 14:09:01','2021-02-18 14:09:01'),
(9,'wew2',NULL,0,'2021-02-18 14:09:41','2021-02-18 14:09:41');

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values 
('iigk5hc316n6du2t9jmfptu7aise40hh','::1',1613657037,'__ci_last_regenerate|i:1613657037;KCFINDER|a:3:{s:8:\"disabled\";b:0;s:9:\"uploadURL\";s:25:\"../images/uploads/tinymce\";s:9:\"uploadDir\";s:0:\"\";}'),
('t7dpevlvbaro1lcglrbbng7focohgt49','::1',1613657363,'__ci_last_regenerate|i:1613657363;KCFINDER|a:3:{s:8:\"disabled\";b:0;s:9:\"uploadURL\";s:25:\"../images/uploads/tinymce\";s:9:\"uploadDir\";s:0:\"\";}'),
('dohr2ucs1sjr45mtiosglt6lh2ilqot0','::1',1613657363,'__ci_last_regenerate|i:1613657363;KCFINDER|a:3:{s:8:\"disabled\";b:0;s:9:\"uploadURL\";s:25:\"../images/uploads/tinymce\";s:9:\"uploadDir\";s:0:\"\";}');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(11,'2014_10_12_000000_create_users_table',1),
(12,'2014_10_12_100000_create_password_resets_table',1),
(13,'2021_02_18_083600_create_building_table',1),
(14,'2021_02_18_083614_create_category_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
