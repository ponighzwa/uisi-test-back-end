<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('backend')->group(function () {
    Route::prefix('category')->group(function (){
        Route::get('/', 'CategoryController@read')->name("category.read");
        Route::get('/{id}', 'CategoryController@show')->name('category.show');
        Route::post('/', 'CategoryController@create')->name("category.create");
        Route::put('/{id}', 'CategoryController@put');
        Route::delete('/{id}', 'CategoryController@remove');
    });

    Route::prefix('building')->group(function (){
        Route::get('/', 'BuildingController@read');
        Route::get('/{id}', 'BuildingController@show');
        Route::post('/', 'BuildingController@create');
        Route::put('/{id}', 'BuildingController@put');
        Route::delete('/{id}', 'BuildingController@remove');
    });
});

Route::get('category', 'CategoryController@index')->name('category.index');
Route::get('building', 'CategoryController@index')->name('building.index');
